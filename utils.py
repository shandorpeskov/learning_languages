from colorama import Fore

def print_green(s):
    print(Fore.GREEN, s, Fore.RESET)

def print_red(s):
    print(Fore.RED, s, Fore.RESET)