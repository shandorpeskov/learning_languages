import fileinput
import re
from random import randint
from enum import Enum, auto
from utils import print_green, print_red

NUM_CORRECTS = 3

class Mode(Enum):
    FR2RUS = auto()
    RUS2FR = auto()
    MIXED = auto()

def resolve_mode():
    mode = input("Press 1 for Fr -> Rus; 2 for Rus -> Fr; 3 for mix: ")

    if mode == '1': return Mode.FR2RUS
    elif mode == '2': return Mode.RUS2FR
    elif mode == '3': return Mode.MIXED
    else: raise Exception("Wrong Input")

def create_dict_to_learn(mode):
    result = []
    #for line in fileinput.input(encoding="utf-8"):
    for line in fileinput.input():
        line = line.rstrip()
        french = re.match("(.*) = (.*)", line).group(1)
        rus = re.match("(.*) = (.*)", line).group(2)
        if mode in (Mode.FR2RUS, Mode.MIXED):
            result += [[french, rus]]
        if mode in (Mode.RUS2FR, Mode.MIXED):
            result += [[rus, french]]
    return result

def learn(mydict, num_corrects):
    # append number of successes = 0 
    for w in mydict: w += [0]

    while (len(mydict)):
        rand_i = randint(0, len(mydict) - 1)
        ans = input("%s: " % mydict[rand_i][0])
        if ans == mydict[rand_i][1]:
            mydict[rand_i][2] += 1
            print_green("Success")
            if mydict[rand_i][2] == num_corrects:
                mydict.pop(rand_i)
        else:
            print_red("Nope, the right answer is: '%s'" % mydict[rand_i][1])
            mydict[rand_i][2] = 0


if __name__ == '__main__':
    # French -> Rus || Rus -> French || Mixed
    mode = resolve_mode()

    mydict = create_dict_to_learn(mode)

    learn(mydict, NUM_CORRECTS)

    print_green("CONGRATULATIONS! YOU'RE DONE!")
    